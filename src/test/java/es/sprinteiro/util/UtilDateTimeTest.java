package es.sprinteiro.util;

import java.util.Date;

import org.junit.Test;

import es.sprinteiro.util.UtilDateTime;

public class UtilDateTimeTest {

	@Test
	public void convertDateFormatToStringTest() {
		String format = "dd-MM-yyyy HH:mm:ss zzz";
		Date date = new Date();
		String result = UtilDateTime.convertDateFormatToString(date, format);
		System.out.println("Result-->" +  result);
	}
}
