package es.sprinteiro.facade.ws.rest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import es.sprinteiro.business.AccountManager;
import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;


public class GatewayControllerTest {

	public GatewayController gateController;
	
	public AccountManager accountManagerMock;
	
	public BindingResult resultMock;
	
	public HttpServletResponse responseMock;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }
	
	@Before
	public void setUp() throws Exception {
		accountManagerMock = mock(AccountManager.class);
		gateController = new GatewayController(accountManagerMock);
		resultMock = mock(BindingResult.class);
		responseMock = mock(HttpServletResponse.class);
	}
	
	@After
	public void tearDown() throws Exception {
		accountManagerMock = null;
		gateController = null;
		resultMock = null;
		responseMock = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Test
	public void shouldCreateOneAccount() throws BindException {
		Account account = AccountMock.createAccountWithoutIdAndExpirityDay();
		
		when(accountManagerMock.newAccount(account)).thenAnswer(new Answer<Account>() {
			@Override
			public Account answer(InvocationOnMock invocation) throws Throwable {
				Account account = (Account) invocation.getArguments()[0];
				account.setId(1111L);

				return account;
			}
		});
		
		gateController.newAccount(account, resultMock, responseMock);
		
		verify(accountManagerMock).newAccount(account);
		
		assertNotNull(account.getId());
		assertEquals(Long.valueOf(1111), account.getId());
	}
	
}
