package es.sprinteiro.facade.ws.rest;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.validation.BindException;

import es.sprinteiro.business.HumanResourceManager;
import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;


public class HumanResourceControllerTest {
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(HumanResourceControllerTest.class);

	private HumanResourceController humanResourceController;
	
	private HumanResourceManager humanResourceManagerMock;
	
	private HttpServletResponse responseMock;
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {	
		
	}
	
	@Before
	public void setUp() throws Exception {
		humanResourceManagerMock = mock(HumanResourceManager.class);
		humanResourceController = new HumanResourceController(humanResourceManagerMock);
		responseMock = mock(HttpServletResponse.class);
	}
	
	@After
	public void tearDown() throws Exception {
		humanResourceManagerMock = null;
		humanResourceController = null;
		responseMock = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test
	public void shouldCreateOnePerson() throws BindException {
		Person person = PersonMock.createPersonWithoutId();

		when(humanResourceManagerMock.newPerson(person)).thenAnswer(new Answer<Person>() {
			@Override
			public Person answer(InvocationOnMock invocation) throws Throwable {
				Person person = (Person) invocation.getArguments()[0];
				person.setId("AABBCC");
				
				return person;
			}
		});
		
		humanResourceController.newPerson(person, responseMock);
		
		verify(humanResourceManagerMock).newPerson(person);
		
		assertEquals("AABBCC", person.getId());
	}
	
	@Test
	public void shouldModifyOneExistingPerson() throws BindException {
		Person person = PersonMock.createPersonWithoutId();

		when(humanResourceManagerMock.newPerson(person)).thenReturn(person);
		
		humanResourceController.modifyPerson(person);
		
		verify(humanResourceManagerMock).modifyPerson(person);
	}
	
	@Test
	public void shouldRemoveOneExistingPerson() throws BindException {
		final String id = "AABBCC";
		
		doNothing().when(humanResourceManagerMock).removePerson(id);
		
		humanResourceController.removePerson(id);
		
		verify(humanResourceManagerMock).removePerson(id);
	}

	@Test
	public void shouldGetOneExistingPersonById() throws BindException {
		Person expected = PersonMock.createPersonWithId();
		
		when(humanResourceManagerMock.findPersonById(expected.getId())).thenReturn(expected);
		
		Person actual = humanResourceController.getPerson(expected.getId());
		
		verify(humanResourceManagerMock).findPersonById(expected.getId());
		
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getAge(), actual.getAge());
		assertEquals(expected.getName(), actual.getName());
	}
	
	@Test
	public void shouldGetExistingPersonsByNameAndAge() throws BindException {
		String nameQuery = "Kuvuni"; int ageQuery = 22;
		
		Person personOne = new Person (nameQuery, ageQuery);
		personOne.setId("AABB01");
		Person personTwo = new Person (nameQuery, ageQuery);
		personTwo.setId("AABB02");

		List<Person> expectedPersonsMock = new ArrayList<Person>(2);
		expectedPersonsMock.add(personOne);
		expectedPersonsMock.add(personTwo);

		when(humanResourceManagerMock.findPersonsByNameAndAge(nameQuery, ageQuery)).thenReturn(expectedPersonsMock);
		
		List<Person> actualPersons = humanResourceController.getPersons(nameQuery, ageQuery);
		
		verify(humanResourceManagerMock).findPersonsByNameAndAge(nameQuery, ageQuery);
		
		assertNotNull(actualPersons);
		assertTrue(actualPersons.size() == 2);
		Set<String> ids = new HashSet<String>(Arrays.asList("AABB01", "AABB02"));
		for (Person person : actualPersons) {
			assertEquals(nameQuery, person.getName());
			assertEquals(ageQuery, person.getAge());
			assertTrue(ids.contains(person.getId()));
		}
	}
	
//	@Test(expected = BindException.class)
//	public void shouldThrowExceptionWhenGetPersonsByNameAndAge() throws BindException {
//		doThrow(BindException.class).when(resultMock).hasErrors();
//
//		humanResourceController.getPersons("AABBCC", 21);		
//	}
}

