package es.sprinteiro.persistence.jpa.entities;

import es.sprinteiro.util.UtilRandomNumber;

//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.junit.Assert;
//import org.junit.Test;

public class CustomerMock {

	public static Customer createCustomerWithoutId() {
		Customer customer = new Customer();
		customer.setFirstName(
			new String[] {"Roberto", "Julio", "Marisa", "Anna"}[UtilRandomNumber.getRandom(0, 4)]
		);
		customer.setLastName(
			new String[] {"Roberto", "Julio", "Marisa", "Anna"}[UtilRandomNumber.getRandom(0, 4)]);

		return customer;
	}	
	
	public static Customer createCustomerWithId() {
		Customer customer = createCustomerWithoutId();
		customer.setId((long) UtilRandomNumber.getRandom(1, 1000));

		return customer;
	}
	
}
