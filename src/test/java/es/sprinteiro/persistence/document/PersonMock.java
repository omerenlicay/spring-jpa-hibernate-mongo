package es.sprinteiro.persistence.document;

public class PersonMock {
	
	public static Person createPersonWithoutId() {
		Person person = new Person();
		person.setName(new String[] {"Alan", "Cris", "Alonso", "Julio"}[getRandom(0, 4)]);
		person.setAge(new int[] {23, 45, 11, 67}[getRandom(0, 4)]);
		
		return person;
	}
	
	public static Person createPersonWithId() {
		Person person = createPersonWithoutId();
		person.setId(new String[] {"AAAAAA", "BBBBBB", "CCCCCC", "DDDDDD"} [getRandom(0, 4)]);
		
		return person;
	}

	private static int getRandom(int min,int max){
		return (int)(Math.random()*(max-min))+min;	
	}
}
