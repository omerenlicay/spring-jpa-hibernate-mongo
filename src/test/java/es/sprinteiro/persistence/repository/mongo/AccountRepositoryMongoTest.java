package es.sprinteiro.persistence.repository.mongo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.Mongo;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class AccountRepositoryMongoTest {
	@SuppressWarnings("unused")
	private final Logger log = Logger.getLogger(AccountRepositoryMongoTest.class);
	
	private AccountRepositoryMongo accountRepositoryMongo;
	
	private MongoOperations mongoTemplateMock;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }
	
	@Before
	public void setUp() throws Exception {
		mongoTemplateMock = mock(MongoOperations.class);
		accountRepositoryMongo = new AccountRepositoryMongo(mongoTemplateMock);
	}
	
	@After
	public void tearDown() throws Exception {
		mongoTemplateMock = null;
		accountRepositoryMongo = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception { }
	
	@Test
	public void shouldCreateOneAccount() {
		final Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		
		doNothing().when(mongoTemplateMock).save(expected);
		when(accountRepositoryMongo.save(expected)).thenAnswer(new Answer<Account>() {
			@Override
			public Account answer(InvocationOnMock invocation) throws Throwable {
				expected.setId(1111L);
				return null;
			}
		});
		
		Account actual = accountRepositoryMongo.save(expected);
		
		verify(mongoTemplateMock).save(expected);
		
		assertNotNull(actual);
		assertNotNull(actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
	}

	@Test
	public void shouldModifyOneAccount() {
		final Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		
		doNothing().when(mongoTemplateMock).save(expected);
		
		Account actual = accountRepositoryMongo.save(expected);
		
		verify(mongoTemplateMock).save(expected);
		
		assertNotNull(actual);
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
	}
	
	@Test
	public void shouldRemoveOneAccountById() {
		Account account = AccountMock.createAccountWithIdAndExpirityDay();
		
		doNothing().when(mongoTemplateMock).remove(account);
		
		accountRepositoryMongo.delete(account);
		
		verify(mongoTemplateMock).remove(account);
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldThoughDataAccessExceptionWhenRemovingOneAccountWithoutId() {
		Account account = AccountMock.createAccountWithoutIdAndExpirityDay();
		
		doThrow(RuntimeException.class).when(mongoTemplateMock).remove(account);
		
		accountRepositoryMongo.delete(account);
	}
	
	@Test
	public void shouldFindOneAccountById() {
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(mongoTemplateMock.findById(expected.getId(), Account.class)).thenReturn(expected);
		
		Account actual = accountRepositoryMongo.findOneById(expected.getId());
		
		verify(mongoTemplateMock).findById(expected.getId(), Account.class);
		
		assertNotNull(actual);
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
	}	
	
//	@Test
	public void shouldFindAccountsByExpirityDateGreaterThan() throws UnknownHostException {
		// TODO: It doesn't work
		Calendar calendar = Calendar.getInstance();
		final Date dateQuery = calendar.getTime();
		calendar.set(2013, 1, 1);
		
		final List<Account> expectedAccounts = new ArrayList<Account>(2);
		expectedAccounts.add(AccountMock.createAccountWithIdAndExpirityDay());
		
		Account account = AccountMock.createAccountWithIdAndExpirityDay();
		calendar.set(2013, 1, 15);
		account.setExpirityDate(calendar.getTime());
		expectedAccounts.add(account);

//		Query spy = spy(query(where("expirityDate").gt(dateQuery)));
		Query queryMock = mock(Query.class);
		
		Mongo mongo = new Mongo();
		
		
		MongoTemplate mongoTemplate = new MongoTemplate(spy(mongo),"dbtest");
		MongoTemplate spyMongoTemplate = spy(mongoTemplate);
		
		doReturn(expectedAccounts).when(spyMongoTemplate).find(spy(queryMock), Account.class);
		
		List<Account> actualAccounts = accountRepositoryMongo.findByExpirityDateGreaterThan(dateQuery);
		
		verify(mongoTemplateMock).find(queryMock, Account.class);
		
		assertNotNull(actualAccounts);
		assertTrue(expectedAccounts.size() == actualAccounts.size());
		for (Account acc : actualAccounts) {
			assertTrue (acc.getExpirityDate().after(dateQuery));
		}
	}	
	
//	@Test
	public void shouldFindAccountsByCustomer_FirstNameAndCustomer_LastName() {
		// TODO
	}	
}
