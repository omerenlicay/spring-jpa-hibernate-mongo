package es.sprinteiro.persistence.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Customer {
	
	private Long id;
	
	private String firstName;
	
	private String lastName;

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Customer [id=").append(getId())
		  .append(", firstName=").append(getFirstName())
		  .append(", lastName=").append(getLastName())
		  .append(']');
		
		return sb.toString();
	}
	
}
