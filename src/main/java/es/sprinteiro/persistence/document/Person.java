package es.sprinteiro.persistence.document;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Person implements Serializable {

	private static final long serialVersionUID = -54107517164610407L;

	private String id;

	private String name;

	private int age;

	public Person() {
	}

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}	  

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Person [id=").append(getId())
		.append(", name=").append(getName())
		.append(", age=").append(getAge())
		.append(']');

		return sb.toString();
	}

}
