package es.sprinteiro.persistence.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.sprinteiro.persistence.jpa.entities.Account;


@Repository
public interface AccountRepositoryJpa extends JpaRepository<Account, Long> {
	List<Account> findByExpirityDateGreaterThan(Date date);
	
	List<Account> findAccountByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName);
}
