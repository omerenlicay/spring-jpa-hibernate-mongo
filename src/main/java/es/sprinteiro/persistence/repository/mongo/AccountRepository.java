package es.sprinteiro.persistence.repository.mongo;

import java.util.Date;
import java.util.List;

import es.sprinteiro.persistence.jpa.entities.Account;


public interface AccountRepository {
	Account save(Account account);
	
	void delete(Account account);

	Account findOneById(Long id);
	
	List<Account> findByExpirityDateGreaterThan(Date date);
	
	List<Account> findByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName);
}
