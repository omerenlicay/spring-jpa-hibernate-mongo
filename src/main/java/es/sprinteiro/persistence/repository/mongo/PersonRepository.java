package es.sprinteiro.persistence.repository.mongo;

import java.util.List;

import es.sprinteiro.persistence.document.Person;

public interface PersonRepository {
	Person save(Person person);
//	Person save(String name, int age);
	
	Person findOneById(String id);
	
	void delete(Person person);
	
	List<Person> findByNameAndAge(String name, int age);
}
