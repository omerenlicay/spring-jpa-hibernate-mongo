package es.sprinteiro.persistence.repository.mongo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import es.sprinteiro.persistence.jpa.entities.Account;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;


// @Component
// Using @Repository instead of @Component because @Repository translate platform-mongo 
// data access exception into generic Spring's data access exception
@Repository
public class AccountRepositoryMongo implements AccountRepository {
	private MongoOperations mongoTemplate;
	
	
	@Autowired
	public AccountRepositoryMongo(MongoOperations mongoTemplate) {
		if (mongoTemplate == null) {
			throw new IllegalArgumentException("MongoTemplate cannot be null.");
		}
		
		this.mongoTemplate = mongoTemplate;
	}
	
	public MongoOperations getMongoTemplate() {
		return mongoTemplate;
	}
	
	public Account save(Account account) {
		mongoTemplate.save(account);
		
		return account;
	}
	
	public void delete(Account account) {
		// Remove by Id
		mongoTemplate.remove(account);
	}

	public Account findOneById(Long id) {
		return mongoTemplate.findById(id, Account.class);
	}
	
	public List<Account> findByExpirityDateGreaterThan(Date date) {
		//TODO
		return mongoTemplate.find(query(where("expirityDate").gt(date)), Account.class);
	}
	
	public List<Account> findByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName) {
		//TODO
		return mongoTemplate.find(
			query(where("customer.firstName").is(firstName).and("customer.lastName").is(lastName)), 
			Account.class);
	}
}
