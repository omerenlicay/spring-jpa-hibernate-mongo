package es.sprinteiro.facade.ws.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.sprinteiro.business.HumanResourceManager;
import es.sprinteiro.persistence.document.Person;


@Controller
@RequestMapping("/hr")
public class HumanResourceController {
	private final Logger log = Logger.getLogger(HumanResourceController.class);
	
	private HumanResourceManager humanResourceManagerService;
	
	
	@Autowired
	public HumanResourceController(HumanResourceManager humanResourceManagerService) {
		if (humanResourceManagerService == null) {
			throw new IllegalArgumentException("HumanResourceManagerService cannot be null.");
		}
		this.humanResourceManagerService = humanResourceManagerService;
	}
	
	public HumanResourceManager getHumanResourceManagerService() {
		return humanResourceManagerService;
	}
	
//	@Autowired
//	public void setHumanResourceManagerService(HumanResourceManagerService humanResourceManagerService) {
//		this.humanResourceManagerService = humanResourceManagerService;
//	}

	@RequestMapping(value="/lookup", method=RequestMethod.GET)
	public @ResponseBody String lookUp() {
			log.info("Lookup from HumanResource Done!");
			return "Lookup from HumanResource Done!!";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	// Change input parameter to java.util.Date type
	public void newPerson(@RequestBody Person person, HttpServletResponse response) {
    	
		person = humanResourceManagerService.newPerson(person);
		
		response.setHeader("Location", "/accounts/" + person.getId());
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void modifyPerson(@RequestBody Person person) {
		
		humanResourceManagerService.modifyPerson(person);
	}
	
	@RequestMapping(value="/person/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void removePerson(@PathVariable("id") String id) {
		
		humanResourceManagerService.removePerson(id);
	}
			
	
	@RequestMapping(value="/person/{id}", method=RequestMethod.GET)
	public @ResponseBody Person getPerson(@PathVariable("id") String id) {
		Person person = this.humanResourceManagerService.findPersonById(id);
		
		return person;
	}

	@RequestMapping(value="/person/{name}/{age}", method=RequestMethod.GET)
	public @ResponseBody List<Person> getPersons(@PathVariable String name, @PathVariable int age) {
		
		List<Person> persons = humanResourceManagerService.findPersonsByNameAndAge(name, age); 
		return persons;
	}
	
}
