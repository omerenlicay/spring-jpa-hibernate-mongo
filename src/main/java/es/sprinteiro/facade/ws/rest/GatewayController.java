package es.sprinteiro.facade.ws.rest;


import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.sprinteiro.business.AccountManager;
import es.sprinteiro.persistence.jpa.entities.Account;


@Controller
@RequestMapping("/gate")
public class GatewayController {
	private Logger log = Logger.getLogger(GatewayController.class);
	
	private AccountManager accountManagerService;
	
	
	@Autowired
	public GatewayController(AccountManager accountManagerService) {
		if (accountManagerService == null) {
			throw new IllegalArgumentException("AccountManagerService cannot be null.");
		}
		
		this.accountManagerService = accountManagerService;
	}

	public AccountManager getAccountManagerService() {
		return accountManagerService;
	}
	
	@RequestMapping(value="/lookup", method=RequestMethod.GET)
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public @ResponseBody String lookUp() {
			log.info("Lookup from Gateway Done!");
			return "Lookup from Gateway Done!!";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	// Change input parameter to java.util.Date type
	public void newAccount(@RequestBody Account account, BindingResult result, HttpServletResponse response) 
		throws BindException {
		
    	if(result.hasErrors()) {
    		throw new BindException(result);
		}
    	
		account = accountManagerService.newAccount(account);
		
		response.setHeader("Location", "/accounts/" + account.getId());
	}
}
