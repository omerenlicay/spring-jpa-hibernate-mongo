package es.sprinteiro.business;

import java.util.Date;
import java.util.List;

import es.sprinteiro.persistence.jpa.entities.Account;


public interface AccountManager {
	Account newAccount(Account account);
	
	Account findAccount(Long id);
	
	Account modifyAccount(Account account);	
	
	void removeAccount(Long id);
	
	List<Account> findAccountByExpirityDateGreaterThan(Date date);
	
	List<Account> findAccountByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName);

}
