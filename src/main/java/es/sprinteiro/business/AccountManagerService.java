package es.sprinteiro.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.repository.jpa.AccountRepositoryJpa;


@Service
//@Component
public class AccountManagerService implements AccountManager {
	private AccountRepositoryJpa accountRepositoryJpa;
	
	
	@Autowired
	public AccountManagerService(AccountRepositoryJpa accountRepositoryJpa) {
		if (accountRepositoryJpa == null) {
			throw new IllegalArgumentException("AccountRepositoryJpa cannot be null.");
		}
		this.accountRepositoryJpa = accountRepositoryJpa;
	}
	
	public AccountRepositoryJpa getAccountRepositoryJpa() {
		return accountRepositoryJpa;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Account newAccount(Account account) {
		
		return accountRepositoryJpa.save(account);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Account findAccount(Long id) {
		return accountRepositoryJpa.findOne(id);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Account modifyAccount(Account account) {
		
		return accountRepositoryJpa.save(account);
	}	
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void removeAccount(Long id) {
		Account persisted = accountRepositoryJpa.findOne(id);
		
		if (persisted == null) {
			// TODO: The account doesn't exist, throw a kind of exception
			throw new RuntimeException("The account doesn't exist (id= + " +  id + ")");
		}
		
		accountRepositoryJpa.delete(persisted);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Account> findAccountByExpirityDateGreaterThan(Date date) {
		return accountRepositoryJpa.findByExpirityDateGreaterThan(date);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public List<Account> findAccountByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName) {
		return accountRepositoryJpa.findAccountByCustomer_FirstNameAndCustomer_LastName(firstName, lastName);
	}
	
}
