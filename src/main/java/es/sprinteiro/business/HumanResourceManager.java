package es.sprinteiro.business;

import java.util.List;

import es.sprinteiro.persistence.document.Person;

public interface HumanResourceManager {
	Person newPerson(Person person);
	
	void modifyPerson(Person person);
	
	void removePerson(String id);
	
	Person findPersonById(String id);
	
	List<Person> findPersonsByNameAndAge(String name, int age);
}
