package es.sprinteiro.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.repository.mongo.PersonRepository;


@Service
//@Component
public class HumanResourceManagerService implements HumanResourceManager{
	private PersonRepository personRepositoryMongo;
	
	@Autowired
	public HumanResourceManagerService(PersonRepository personRepositoryMongo) {
		if (personRepositoryMongo == null) {
			throw new IllegalArgumentException("PersonRepositoryMongo cannot be null.");
		}
		
		this.personRepositoryMongo = personRepositoryMongo;
	}
	
	public PersonRepository getPersonRepositoryMongo() {
		return personRepositoryMongo;
	}
	
	@Override
	public Person newPerson(Person person) {
		return personRepositoryMongo.save(person);
	}	
	
	@Override
	public void modifyPerson(Person person) {
		Person personPersisted = personRepositoryMongo.findOneById(person.getId());
		personPersisted.setName(person.getName());
		personPersisted.setAge(person.getAge());
		personRepositoryMongo.save(person);
	}
	
	@Override
	public void removePerson(String id) {
		personRepositoryMongo.delete(personRepositoryMongo.findOneById(id));
	}
	
	@Override
	public Person findPersonById(String id) {
		return personRepositoryMongo.findOneById(id);
	}
	
	@Override
	public List<Person> findPersonsByNameAndAge(String name, int age) {
		return personRepositoryMongo.findByNameAndAge(name, age);
	}
}
