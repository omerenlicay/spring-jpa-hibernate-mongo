package es.sprinteiro.business;

import java.util.Date;
import java.util.List;

import es.sprinteiro.persistence.jpa.entities.Account;

public interface AccountDocumentManager {
	Account newAccountDocument(Account expected);
	
	Account findAccountDocument(Long id);
	
	Account modifyAccountDocument(Account account);
	
	void removeAccountDocument(Long id);

	List<Account> findAccountDocumentByExpirityDateGreaterThan(Date date);
	
	List<Account> findAccountDocumentByCustomer_FirstNameAndCustomer_LastName(String firstName, String lastName);
}
