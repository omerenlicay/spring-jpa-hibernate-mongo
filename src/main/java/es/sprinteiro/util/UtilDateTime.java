package es.sprinteiro.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class UtilDateTime {

	public static String convertDateFormatToString(Date date, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		
		return formatter.format(date);
	}
}
