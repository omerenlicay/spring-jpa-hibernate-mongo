package es.sprinteiro.persistence.repository.mongo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:test-spring-persistence-mongo.xml"})
@ActiveProfiles("default")
public class PersonRepositoryMongoIT {
	private final Logger log = Logger.getLogger(PersonRepositoryMongoIT.class);
	
	private PersonRepositoryMongo personRepositoryMongo;
	
	
	@Autowired
	public void setPersonRepositoryMongo(PersonRepositoryMongo personRepositoryMongo) {
		this.personRepositoryMongo = personRepositoryMongo;
	}
	
	@Test
	public void createReadUpdateDeleteOnePersonTest() {
		log.info("Begining CRUD...");
		Person expected = PersonMock.createPersonWithoutId();
		
		// Create
		Person actual = personRepositoryMongo.save(expected);
		assertNotNull(actual);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
		assertNotNull(expected.getId());
		
		// Update
		final String newName = !actual.getName().equals("Jennifer") ? "Jennifer" : "John";
		final int newAge = actual.getAge() != 23 ? 23 : 40;
		actual.setName(newName);
		actual.setAge(newAge);
		actual = personRepositoryMongo.save(actual);
		assertNotNull(actual);
		assertEquals(newName, actual.getName());
		assertEquals(newAge, actual.getAge());
		
		// Delete
		personRepositoryMongo.delete(actual);
		assertNull(personRepositoryMongo.findOneById(actual.getId()));
		
		log.info("CRUD successfully finished!");
	}
	
	@Test
	public void findOnePersonAfterCreatedAndLaterRemovedByNameAndAge() {
		final Person person = new Person("Federico", 23);
		log.info("Begining findPersons by name (Federico) and age (23)...");
		
		personRepositoryMongo.save(person);
		
		List<Person> persons = 
			personRepositoryMongo.findByNameAndAge(person.getName(), person.getAge());
		assertNotNull(persons);
		assertTrue(persons.size() == 1);
		assertEquals("Federico", persons.get(0).getName());
		assertEquals(23, persons.get(0).getAge());
		
		personRepositoryMongo.delete(person);
		log.info("findPersons by name and age successfully finished!");
	}

	@Test
	public void findTwoPersonAfterCreatedAndLaterRemovedByNameAndAge() {
		final Person person = new Person("Federico", 23);
		log.info("Begining findPersons by name (Federico) and age (23)...");
		
		personRepositoryMongo.save(person);
		person.setId(null);
		personRepositoryMongo.save(person);
		
		List<Person> persons = 
			personRepositoryMongo.findByNameAndAge(person.getName(), person.getAge());
		assertNotNull(persons);
		assertTrue(persons.size() == 2);
	
		for (Person p : persons) {
			assertEquals("Federico", p.getName());
			assertEquals(23, p.getAge());
			personRepositoryMongo.delete(p);
			assertNull(personRepositoryMongo.findOneById(p.getId()));
		}
		
		
		log.info("findPersons by name and age successfully finished!");
	}
	
}
