package es.sprinteiro.business;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;
import es.sprinteiro.util.UtilDateTime;



// IMPORTANT
// AS DEFAULT IF NOT DEFINED Spring's TestExecutionListener
// registers TransactionalTestExecutionListener for transactional test execution support with default rollback semantics
// apart from DependencyInjectionTestExecutionListener, DirtiesContextTestExecutionListener by default too
// (support for dependency injection of the test instance, handling of dirty context)
// http://static.springsource.org/spring/docs/3.0.0.M3/reference/html/ch10s03.html
// So, to indicate to TransactionalTestExecutionListener the change of rollback semantic 
// annotate method with @Rollback(false) if method-level definition
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-spring-business.xml", 
	"classpath:test-spring-persistence-jpa.xml", "classpath:test-spring-persistence-mongo.xml"})
@Transactional
@ActiveProfiles("default")
public class AccountManagerServiceIT {
	private final Logger log = Logger.getLogger(AccountManagerServiceIT.class);

	private AccountManager accountManagerService;
	
	
	@Autowired
	public void setAccountManagerService(AccountManager accountManagerService) {
		this.accountManagerService = accountManagerService;
	}
	
	@Test
//	@Rollback(false)
	public void createReadUpdateDeleteOneAccountTest() {
		log.info("Begining CRUD...");
		
		Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		createReadUpdateDeleteOneAccountTestLauncher(expected);
		
		log.info("CRUD succesfully finished!");
	}

	@Test
	public void createReadUpdateDeleteOneAccountWithCustomerTest() {
		log.info("Begining CRUD...");
		Account expected = AccountMock.createAccountWithCustomerWithoutIdAndExpirityDay();

		createReadUpdateOneAccountTestLauncher(expected);
		
		assertNotNull(expected.getCustomer());
		assertNotNull(expected.getCustomer().getId());
		assertNotNull(expected.getCustomer().getFirstName());
		assertNotNull(expected.getCustomer().getLastName());
		
		deleteOneAccountTestLauncher(expected);
		
		log.info("CRUD succesfully finished!");
	}	
	
	
	@Test
	public void findAccountAfterCreatedAndLaterRemovedByExpirityDate() {
		Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		Account actual = accountManagerService.newAccount(expected);
		assertNotNull(actual);
		
		// Patterns: "dd/MM/yyyy", "dd/MM/yyyy HH:mm, zzz"
		List<Account> actualList = findAccountByExpirityDate(expected.getExpirityDate(), "dd/MM/yyyy");
		
		assertNotNull(actualList);
		assertTrue(actualList.size() > 0);
		
		accountManagerService.removeAccount(actual.getId());
		
		assertNull(accountManagerService.findAccount(actual.getId()));
	}	
	
	@Test
	public void findAccountAfterCreatedAndLaterRemovedByCustomerFirstAndLastName() {
		Account expected = AccountMock.createAccountWithCustomerWithoutIdAndExpirityDay();
		Account actual = accountManagerService.newAccount(expected);
		assertNotNull(actual);
		
		List<Account> actualList = 
			accountManagerService.findAccountByCustomer_FirstNameAndCustomer_LastName(
					expected.getCustomer().getFirstName(), expected.getCustomer().getLastName());
		assertNotNull(actualList);
		
		accountManagerService.removeAccount(actual.getId());		
	}
	
	@Test(expected=RuntimeException.class)
	public void deleteNonExistentAccount() {
		accountManagerService.removeAccount(-999L);
	}
	
	private void createReadUpdateOneAccountTestLauncher(Account expected) {
		// Create
		Long expectedId = accountManagerService.newAccount(expected).getId();
		
		// Read
		Account actual = accountManagerService.findAccount(expectedId);
		assertNotNull(expected);
		assertEquals(expected.getId(), actual.getId());
		String expectedDateTime = UtilDateTime.convertDateFormatToString(expected.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		String actualDateTime = UtilDateTime.convertDateFormatToString(actual.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		assertEquals(expectedDateTime, actualDateTime);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(2012, 11, 11);
				
		actual.setExpirityDate(calendar.getTime());
		// Update
		accountManagerService.modifyAccount(actual);
		actual = accountManagerService.findAccount(expectedId);
		assertEquals(expected.getId(), actual.getId());
		expectedDateTime = UtilDateTime.convertDateFormatToString(calendar.getTime(), "dd-MM-yyyy HH:mm:ss zzz");
		actualDateTime = UtilDateTime.convertDateFormatToString(actual.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		assertEquals(expectedDateTime, actualDateTime);			
	}

	private void deleteOneAccountTestLauncher(Account expected) {
		// Delete
		accountManagerService.removeAccount(expected.getId());
		assertNull(accountManagerService.findAccount(expected.getId()));		
	}
	
	private void createReadUpdateDeleteOneAccountTestLauncher(Account expected) {
		createReadUpdateOneAccountTestLauncher(expected);
		deleteOneAccountTestLauncher(expected);
	}		
	
	private List<Account> findAccountByExpirityDate(final Date date, final String pattern) {
		String expectedDateTime = UtilDateTime.convertDateFormatToString(date, pattern);
		List<Account> accounts = null;
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			accounts = accountManagerService.findAccountByExpirityDateGreaterThan(dateFormat.parse(expectedDateTime));
		} catch (ParseException e) {
			throw new IllegalArgumentException("Datetime string not correct", e);
		}
		
		log.info("Accounts by " + expectedDateTime);
		log.info(accounts);
		
		return accounts;
	}
	
}
