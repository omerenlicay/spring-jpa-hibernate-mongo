package es.sprinteiro.business;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;
import es.sprinteiro.util.UtilRandomNumber;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:test-spring-business.xml", 
		"classpath:test-spring-persistence-jpa.xml", "classpath:test-spring-persistence-mongo.xml"})
@ActiveProfiles("default")
public class HumanResourceManagerServiceIT {
	private final Logger log = Logger.getLogger(HumanResourceManagerServiceIT.class); 
	
	private HumanResourceManager humanResourceManager;
	
	
	@Autowired
	public void setHumanResourceManagerService(HumanResourceManager humanResourceManagerService) {
		this.humanResourceManager = humanResourceManagerService;
	}
	
	@Test
	public void createReadUpdateDeleteOneAccountTest() {
		log.info("Begining CRUD...");
		
		Person expected = PersonMock.createPersonWithoutId();
		// Create
		Person actual = humanResourceManager.newPerson(expected);
		
		assertNotNull(actual);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
		assertNotNull(actual.getId());
		
		// Read
		actual = humanResourceManager.findPersonById(actual.getId());
		assertNotNull(actual);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
		assertNotNull(actual.getId());
		
		// Update
		final String newName = !actual.getName().equals("Jeniffer") ? "Jeniffer" : "John";
		final int newAge = actual.getAge() != 23 ? 23 : 40;
		actual.setName(newName);
		actual.setAge(newAge);
		humanResourceManager.modifyPerson(actual);
		actual = humanResourceManager.findPersonById(actual.getId());
		assertEquals(newName, actual.getName());
		assertEquals(newAge, actual.getAge());
		
		// Delete
		humanResourceManager.removePerson(actual.getId());
		assertNull(humanResourceManager.findPersonById(actual.getId()));
		
		log.info("CRUD successfully finished!");
	}
	
	@Test
	public void findOnePersonAfterCreatedAndLaterRemovedByNameAndAge() {
		final Person person = newPerson(null, 0);
		log.info("Begining findPersons by name (" + person.getName() + 
			") and age (" + person.getAge() + ")...");
		
		List<Person> persons = 
			humanResourceManager.findPersonsByNameAndAge(person.getName(), person.getAge());
		assertNotNull(persons);
		assertTrue(persons.size() == 1);
		
		humanResourceManager.removePerson(persons.get(0).getId());
		log.info("findPersons by name and age successfully finished!");
	}

	@Test
	public void findTwoPersonAfterCreatedAndLaterRemovedByNameAndAge() {
		newPerson("Federico", 43);
		final Person person = newPerson("Federico", 43);
		log.info("Begining...");
		log.info("\tfindPersons by name (Federico) and age (43)...");
		List<Person> persons = 
			humanResourceManager.findPersonsByNameAndAge(person.getName(), person.getAge());
		assertNotNull(persons);
		assertTrue(persons.size() == 2);
		
		humanResourceManager.removePerson(persons.get(0).getId());
		humanResourceManager.removePerson(persons.get(1).getId());
		log.info("findPersons by name and age " +
				"successfully finished ( " + persons + ")!");
	}

	private Person newPerson(String name, int age) {
		log.info("Begining new person creation...");
		String expectedName = name != null ? name :
			new String[] {"Alan", "Emma", "Crystal", "Sharda", "Neel"}[UtilRandomNumber.getRandom(0, 5)];
		int expectedAge = (age != 0) ? age :
			new int[] {11, 34, 67, 87, 33} [UtilRandomNumber.getRandom(0, 5)];
		Person expected = new Person(expectedName, expectedAge);
		Person actual = humanResourceManager.newPerson(expected);
		
		assertNotNull(actual);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
		
		log.info("New person creation succesfully finished(" + actual + ")!");
		
		return actual;
	}	
	
}
